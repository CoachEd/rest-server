# Sample Rest Server              

This is a sample RESTful web server written in Node.js. It has HTTP GET, PUT, POST, and DELETE services. Resources are saved as JSON files (e.g., 0.json). To reset the record counter to 0, just delete counter.json.

## Getting Started

1. Clone this repository.
1. Download and install [Node.js](https://nodejs.org/en/)
1. Install the required Node.js modules: go into the rest-server directory and run `npm install`
1. The server defaults to port 1234. You can edit config.json to change the port number.

### Running the Server

```
$ node app.js
server running: http://localhost:1234/  (Ctrl+C to Quit)
```

### Testing the server from the command line

#### Add a record

```
user@yourmachine:~$  curl -i -H "Content-Type: application/json" -X POST -d '{"subject": "television","message": "i love television","channel":9}' http://localhost:1234/api/v1/addrecord
```

#### Update a record

```
user@yourmachine:~$  curl -i -H "Content-Type: application/json" -X PUT -d '{"subject": "new television","message": "new i love television","channel":99}' http://localhost:1234/api/v1/records/1.json
```

#### Get a record

```
user@yourmachine:~$  curl -i --request GET http://localhost:1234/api/v1/records/1.json
```

#### Delete a record

```
user@yourmachine:~$  curl -i --request DELETE http://localhost:1234/api/v1/records/1.json
```

