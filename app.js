var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var fs = require('fs');
var nconf = require('nconf');

var cfile = 'config.json';

//Validate the incoming JSON config file
try {
	var content = fs.readFileSync(cfile,'utf8');
	var myjson = JSON.parse(content);
	//config file is valid
} catch (ex) {
	console.log("Error in " + cfile);
	console.log('Exiting...');
	console.log(ex);
	process.exit(1);
}

nconf.file({file: cfile});
var configobj = JSON.parse(fs.readFileSync(cfile,'utf8'));

// Start the server
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({type: 'application/vnd/api+json'}));

var routes = require('./routes/routes.js')(app,fs,nconf.get('port'));
var httpServer = http.createServer(app);
httpServer.listen(parseInt(nconf.get('port')));
console.log('server running: http://localhost:%s/  (Ctrl+C to Quit)', parseInt(nconf.get('port')));

// Handle Ctrl-C (graceful shutdown)
process.on('SIGINT', function() {
  console.log('Exiting...');
  process.exit(0);
});