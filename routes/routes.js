/**
* Defines the different RESTful web services that can be called.
*
* @param (type) app Instance of express
* @returns (undefined) Not used
*/

var appRouter = function(app,fs,port) {
  var filepath = 'data';
  
  //Hello service
  app.get('/', function(req, res) {
    return res.status(200).send({'message': 'Hello.'});
  });
  
  //ADD a record
  app.post('/api/v1/addrecord',function(req, res) {
    console.log("HTTP POST (add) received.");
    
    //if counter.json does not exist, create it with counter value 0
    if (!fs.existsSync(filepath + '/counter.json')) {
      console.log('creating counter.json first time...');
      fs.writeFileSync(filepath + '/counter.json', JSON.stringify({ "counter": 0 }, null, 2) , 'utf-8');
    }

    var obj = JSON.parse(fs.readFileSync(filepath + '/counter.json', 'utf8'));
    var newrecordid = obj.counter + 1;
    var dte = new Date().toISOString().replace(/\..+/, '') + 'Z';
    var responseJson = {
      "id": newrecordid,
      "created_at": dte,
      "updated_at": dte,
      "subject": req.body.subject,
      "message": req.body.message,
      "channel": req.body.channel
    };
    
    //write record to file
    fs.writeFile(filepath + '/' + newrecordid + '.json', JSON.stringify(responseJson, null, 2) , 'utf-8');
    
    //update counter file
    fs.writeFile(filepath + '/counter.json', JSON.stringify({ "counter": newrecordid }, null, 2) , 'utf-8');
    console.log('created ' + newrecordid + '.json');
    
    return res.status(200).send(responseJson);
  });
  
  //UPDATE a record
  app.put('/api/v1/records/:id.json',function(req, res) {
    console.log("HTTP PUT (update) received.");        
    
    var recordid = req.params.id;

    // does file exist?
    if (!fs.existsSync(filepath + '/' + recordid + '.json')) {
      console.log('file does not exist: ' + filepath + '/' + recordid + '.json');
      return res.status(404).send({'message': recordid + '.json not found'});            
    }        
    var dte = new Date().toISOString().replace(/\..+/, '') + 'Z';
    var responseJson = JSON.parse(fs.readFileSync(filepath + '/' + recordid + '.json', 'utf8'));

    responseJson.subject = req.body.subject;
    responseJson.message = req.body.message;
    responseJson.channel = req.body.channel;
    responseJson.updated_at = dte;
    
    //write to file
    fs.writeFile(filepath + '/' + recordid + '.json', JSON.stringify(responseJson, null, 2) , 'utf-8');

    return res.status(200).send(responseJson);
  });
  
  //GET a record
  app.get('/api/v1/records/:id.json', function(req, res) {
    console.log("HTTP GET (get) received.");
    
    var recordid = req.params.id;
    
    //if {id}.json file does not exist...
    if (!fs.existsSync(filepath + '/' + recordid + '.json')) {
      console.log('file does not exist: ' + filepath + '/' + recordid + '.json');
      return res.status(404).send({'message': recordid + '.json not found'});
    }        
    
    var retrievedJson = JSON.parse(fs.readFileSync(filepath + '/' + recordid + '.json', 'utf8'));

    res.status(200).send(retrievedJson);
  }); 

  //DELETE a record
  app.delete('/api/v1/records/:id.json', function (req, res) {
    console.log("HTTP DELETE (delete) received.");
    
    var recordid = req.params.id;

    // does file exist?
    if (!fs.existsSync(filepath + '/' + recordid + '.json')) {
      console.log('file does not exist: ' + filepath + '/' + recordid + '.json');
      return res.status(404).send({'message': recordid + '.json not found'});            
    }
   
    fs.unlinkSync(filepath + '/' + recordid + '.json');
    res.status(200).send({'message': 'success'});
  });
}

module.exports = appRouter;
